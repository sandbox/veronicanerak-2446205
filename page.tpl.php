<div id="wrapper">

	<header id="header">
	
    <?php if ($main_menu): ?>
			<nav class="nav-container clearfix" id="nav-topbar">
				<div class="nav-toggle"><i class="fa fa-bars"></i></div>
				<div class="nav-text"><!-- put your mobile menu text here --></div>
        <div class="nav-wrap container">
          <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix', 'nav', 'container-inner' )), 'heading' => '')); ?>
        </div>
				
        <?php //TODO: IF: setting to print Search form is true: ?>
				<div class="container">
					<div class="container-inner">		
						<div class="toggle-search"><i class="fa fa-search"></i></div>
						<div class="search-expand">
							<div class="search-expand-inner">
								<?php //get_search_form(); ?>
							</div>
						</div>
					</div><!--/.container-inner-->
				</div><!--/.container-->

        <?php //TODO: ELSE: print the header region: ?>

        <?php if ($page['header']): ?>
				<div class="container">
					<div class="container-inner">		
            <?php print render($page['header']); ?>
					</div><!--/.container-inner-->
				</div><!--/.container-->
        <?php endif; ?>
				
			</nav><!--/#nav-topbar-->
		<?php endif; ?>
		
		<div class="container clearfix">
			<div class="container-inner">
				
				<div class="clearfix pad">

          <h1 class="site-title grid one-third">
            <a href="<?php print $base_path; ?>" rel="home">
              <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>">
            </a>
          </h1>

           <?php if ($page['advertisement']): ?>
            <div class="advertisement-region grid one-half">
              <div class="advertisement-inner">   
                <?php print render($page['advertisement']); ?>
              </div><!--/.advertisement-inner-->
            </div><!--/.container-->
          <?php endif; ?>

          <?php if($site_slogan): ?>
					<p class="site-description"><?php print $site_slogan; ?></p><?php //endif; ?>
          <?php endif; ?>

				</div>
				
        <?php if ($main_menu): ?>
        <nav class="nav-container clearfix" id="nav-header">

          <div class="nav-toggle"><i class="fa fa-bars"></i></div>

          <div class="nav-text"><!-- put your mobile menu text here --></div>

          <div class="nav-wrap container">

  <?php print theme('links', array('links' => menu_navigation_links(theme_get_setting('menu_picker')), 'attributes' => array('id' => 'custom-menu', 'class'=> array('links', 'inline', 'clearfix', 'nav', 'container-inner')))); ?>
          </div>


        </nav><!--/#nav-header-->
        <?php endif; ?>
				
		</div><!--/.container-->
		
	</header><!--/#header-->
	
	<div class="container" id="page">
		<div class="container-inner">			
			<div class="main">
				<div class="main-inner clearfix">

          <section class="content">

            <?php print $messages; ?>

            <?php if ($tabs_rendered = render($tabs)): ?><div class="tabs"><?php print $tabs_rendered; ?></div><?php endif; ?>

            <?php print render($title_prefix); ?>
            <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
            <?php print render($title_suffix); ?>

            <?php print render($page['help']); ?>

            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

            
            <div class="pad group">
              
              <?php if ($page['featured']): ?>
                <div class="featured">
                  <?php print render($page['featured']); ?>
                </div> <!-- /.featured -->
              <?php endif; ?>
              
              <div class="post-list group">
                <div class="post-row">
                  <?php print render($page['content']); ?>
                </div><!--/.post-row-->
              </div><!--/.post-list-->
              
            </div><!--/.pad-->
            
          </section><!--/.content-->

          <?php if ($page['sidebar_first']): ?>
            <div class="sidebar s1">
              <div class="sidebar-content">

                <?php //TODO: should be replaced with dynamic theme settings ?>
                <div class="sidebar-top clearfix">
                  <p><?php print t('Follow'); ?>:</p><?php //TODO: change this string from settings ?>

                  <ul class="social-links">
                    <li><a rel="nofollow" class="social-tooltip" title="Twitter" href="<?php print theme_get_setting('twitter'); ?>"><i class="fa fa-twitter"></i></a></li>
                    <li><a rel="nofollow" class="social-tooltip" title="Facebook" href="<?php print theme_get_setting('facebook'); ?>"><i class="fa fa-facebook"></i></a></li>
                    <li><a rel="nofollow" class="social-tooltip" title="Google+" href="<?php print theme_get_setting('gplus'); ?>"><i class="fa fa-google-plus"></i></a></li>
                    <li><a rel="nofollow" class="social-tooltip" title="<?php print theme_get_setting('custom_social_label'); ?>" href="<?php print theme_get_setting('custom_social_url'); ?>"><i class="fa fa-<?php print theme_get_setting('custom_social_icon'); ?>"></i></a></li>
                    </ul>
                </div><!-- /.sidebar-top -->

                <?php print render($page['sidebar_first']); ?>
              </div><!--/.sidebar-content-->
            </div><!--/.sidebar-->
          <?php endif; ?>
          

          <?php if ($page['sidebar_second']): ?>
            <div class="sidebar s2">
              <div class="sidebar-content">
                <div class="sidebar-top clearfix">
                  <p><?php print t ('More'); ?></p><?php //TODO: change this string from settings ?>
                </div>
                <?php print render($page['sidebar_second']); ?>
              </div><!--/.sidebar-content-->
            </div><!--/.sidebar-->
          <?php endif; ?>

				</div><!--/.main-inner-->
			</div><!--/.main-->			
		</div><!--/.container-inner-->
	</div><!--/.container-->

	<footer id="footer">
		
    <?php if ($page['content_bottom_1'] || $page['content_bottom_2'] || $page['content_bottom_3']): ?>

		<section class="container" id="footer-ads">
			<div class="container-inner">
				<?php //dynamic_sidebar( 'footer-ads' ); ?>
			</div><!--/.container-inner-->
		</section><!--/.container-->
		<?php //endif; ?>
		
		<section class="container" id="footer-widgets">
			<div class="container-inner">
				
				<div class="pad clearfix">

          <div class="footer-widget-1 grid one-third ">
            <?php if ($page['content_bottom_1']): ?>
              <?php print render($page['content_bottom_1']); ?>
            <?php endif; ?>
          </div>

          <div class="footer-widget-2 grid one-third ">
            <?php if ($page['content_bottom_2']): ?>
              <?php print render($page['content_bottom_2']); ?>
            <?php endif; ?>
          </div>

          <div class="footer-widget-3 grid one-third last">
            <?php if ($page['content_bottom_3']): ?>
              <?php print render($page['content_bottom_3']); ?>
            <?php endif; ?>
          </div>

				</div><!--/.pad-->
				
			</div><!--/.container-inner-->
		</section><!--/.container-->	
		<?php endif; ?>
		
    <?php if ($main_menu): ?>
			<nav class="nav-container clearfix" id="nav-footer">
				<div class="nav-toggle"><i class="fa fa-bars"></i></div>
				<div class="nav-text"><!-- put your mobile menu text here --></div>
        <div class="nav-wrap">
          <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix', 'nav', 'container-inner' )), 'heading' => '')); ?>
        </div>
			</nav><!--/#nav-footer-->
		<?php endif; ?>
		
		<section class="container" id="footer-bottom">
			<div class="container-inner">
				
				<a id="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>
				
				<div class="pad clearfix">
					
					<div class="grid one-half">
						
						<?php //if ( ot_get_option('footer-logo') ): ?>
              <!--<img id="footer-logo" src="<?php //print $logo; ?>" alt="<?php //print $site_name; ?>">-->
						<?php //endif; ?>
            
            <?php if(theme_get_setting('footer_logo')): ?>
              <img id="footer-logo" src="<?php print $logo; ?>" alt="<?php print $site_name; ?>">
            <?php endif; ?>
						
					</div>
					
					<div class="grid one-half last">	
              Social links goes here
					</div>
				
          <?php if ($page['footer']): ?>
            <?php print render($page['footer']); ?>
          <?php endif; ?>

				</div><!--/.pad-->

        <?php print $feed_icons; ?>
				
			</div><!--/.container-inner-->
		</section><!--/.container-->
		
	</footer><!--/#footer-->
</div><!--/#wrapper-->

</body>
</html>
