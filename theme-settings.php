<?php  


/**
 * Implementing theme_form_system_theme_settings_alter()
 **/
function huemandrupal_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['huemandrupal_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Hueman Drupal Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -15,
  );


  //Menu settings fieldset
  $form['huemandrupal_settings']['menu_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu Settings')
  );
 
  //Implementing settings to select a menu and print it on the top menu area

  //If there is a stored value check that as default
  if (theme_get_setting('menu_picker')){
    $menu = theme_get_setting('menu_picker');
  } 
  //Otherwise print the secondary menu
  else {
    $menu = 'navigation';
  }

  $form['huemandrupal_settings']['menu_settings']['menu_picker'] = array(
      '#type' => 'select',
      '#title' => t('Choose a menu'),
      '#description' => t('Choose the menu to be printed bellow the main logo'),
      '#default_value' => $menu,
      '#options' => menu_get_menus(),
    );

  //Footer options
  $form['huemandrupal_settings']['footer_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Footer options'),
    '#default_value' => theme_get_setting('footer_options'),
  );

  //Option to choose if the logo should be printed on the footer
  $form['huemandrupal_settings']['footer_options']['footer_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the default logo on the footer region'),
    '#default_value' => theme_get_setting('footer_logo'),
  );

  //
  $form['huemandrupal_settings']['social'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social options')
  );

  //Facebook
  $form['huemandrupal_settings']['social']['facebook'] = array(
      '#type' => 'textfield',
      '#title' => t('Facebook profile URL'),
      '#description' => t('Insert your Facebook profile URL here. Example: http://facebook.com/example'),
      '#default_value' => theme_get_setting('facebook'),
  );

  //Twitter
  $form['huemandrupal_settings']['social']['twitter'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter profile URL'),
      '#description' => t('Insert your Twitter profile URL here. Example: http://twitter.com/example'),
      '#default_value' => theme_get_setting('twitter'),
  );

  //Google Plus
  $form['huemandrupal_settings']['social']['gplus'] = array(
      '#type' => 'textfield',
      '#title' => t('Google Plus profile URL'),
      '#description' => t('Insert your Google Plus profile URL here. Example: https://plus.google.com/example'),
      '#default_value' => theme_get_setting('gplus'),
  );

  //Other
  $form['huemandrupal_settings']['social']['other'] = array(
      '#type' => 'fieldset',
      '#title' => t('Other'),
      '#description' => t('Insert an additional social network or link with an icon'),
  );

  $form['huemandrupal_settings']['social']['other']['custom_social_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#description' => t('The label to appear when hover the icon. Example: Linkedin'),
      '#default_value' => theme_get_setting('custom_social_label'),
  );

  $form['huemandrupal_settings']['social']['other']['custom_social_url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('Insert a URL to link the icon. Example: http://linkedin.com/jhonsmith'),
      '#default_value' => theme_get_setting('custom_social_url'),
      '#rules' => array( 'url' )
  );

  $form['huemandrupal_settings']['social']['other']['custom_social_icon'] = array(
      '#type' => 'textfield',
      '#title' => t('Icon'),
      '#description' => t('Insert the font awesome icon name here. Example: rss-square. Do not add the "fa-" prefix.'),
      '#default_value' => theme_get_setting('custom_social_icon'),
  );
}
